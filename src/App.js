import ProductsContainer from "./components/ProductsContainer";
import Header from "./components/Header";
import { createContext, useReducer, useMemo } from "react";

export const reducer = (likedProducts, product) => {
  if (likedProducts.find((i) => i.id === product.id)) {
    return likedProducts.filter((i) => i.id !== product.id);
  } else {
    return [...likedProducts, product];
  }
};

export const AppContext = createContext({
  likedProducts: [],
  setLikedProducts: () => {},
});

function App() { 

  const [ likedProducts, setLikedProducts ] = useReducer(reducer, []);

  const contextValue = useMemo(
    () => ({ likedProducts, setLikedProducts }), 
    [ likedProducts ],
  );

  return (
    <AppContext.Provider value={contextValue}>
      <Header/>
      <ProductsContainer/>
    </AppContext.Provider>
  );
}

export default App;
