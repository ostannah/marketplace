import { useEffect, useState } from "react";
import data from '../api/data.json';

export const useGetProducts = (url) => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        // const res = await fetch(url);
        // if (!res.ok) {
        //   throw new Error(res.statusText);
        // }
        // const data = await res.json();
        setProducts(data);
      } catch (exception) {
        console.log('Uh oh, an error occurs whilst fetching products...')
      } finally {
          setLoading(false);
      }
    };

    fetchData();
  }, [url]);

  return { products, loading };
};
