/* eslint-disable testing-library/prefer-screen-queries */
/* eslint-disable testing-library/render-result-naming-convention */
import { render } from '@testing-library/react';
import { ProductCard } from '../components/ProductCard';

test("displays a default thumbnail", async () => {
  const productCard = render(<ProductCard />);
  const productCardThumbnail = await productCard.findByTestId("thumbnail");
  expect(productCardThumbnail.src).toContain("depop_logo.png");
});

// test("displays an image", async () => {
//   const product = {
//     img: 'img.png',
//   };
//   const productCard = render(<ProductCard product={product}/>);
//   const productCardThumbnail = await productCard.findByTestId("thumbnail");
//   expect(productCardThumbnail.src).toContain("img.png");
// });
