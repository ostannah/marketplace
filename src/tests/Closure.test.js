
const outerFunction = (intArg) => {
  let count = intArg;
  const innerFunction = () => {
    count = count + 1;
    return count;
  }
  return innerFunction;
};

test("New function equals 1", () => {
  const newFunction = outerFunction(0);
  let result;
  result = newFunction()

  expect(result).toBe(1);
});

test("New function equals 2", () => {
  const newFunction = outerFunction(0);
  let result;
  result = newFunction();
  result = newFunction();

  expect(result).toBe(2);
});
