
test("Second function inherits methods from first funciton", () => {
  function firstFunction() {};
  firstFunction.prototype.doSomething =  () => 'doSomething';

  const secondFunction = new firstFunction();
  expect(secondFunction.doSomething()).toBe('doSomething');
});