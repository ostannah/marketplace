import { render } from "@testing-library/react";
import { useGetProducts } from "../hooks/useGetProducts";
import { renderHook } from "@testing-library/react-hooks";

// custom hooks cannot be called outside components so must implement a fake component
const getProducts = () => {
  let products;

  const TestComponent = () => {
    products = useGetProducts();
    return null;
  }
  render(<TestComponent/>);
  return products;
};


test("returns products and loading state", async () => {
  const { products, loading } = getProducts();
  expect(products).toHaveLength(0);
  expect(loading).toBe(false);
});

test("returns products and loading state v2 ", async () => {
  const { result } = renderHook(() => useGetProducts());

  const { products, loading } = result.current;

  expect(products).toHaveLength(0);
  expect(loading).toBe(false);
});

// test("returns products ", async () => {
//   const mockProducts = [
//     { id: '1'},
//     { id: '2'},
//     { id: '3'},
//   ];
//   fetch.mockResponseOnce(
//     JSON.stringify({
//       mockProducts,
//     })
//   );
//   const { result, waitForNextUpdate } = renderHook(() => useGetProducts());

//   await waitForNextUpdate();

//   const { products, loading } = result.current;

//   expect(products).toHaveLength(0);
//   expect(loading).toBe(false);
// });
