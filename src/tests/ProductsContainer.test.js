import { render } from '@testing-library/react';
import { create } from "react-test-renderer";
import { ProductsContainer } from "../components/ProductsContainer";

const products = [
  {
    id: "1", 
    date: "2021-02-21T20:17:26.366Z",
    name: "Arch Blick",
    img: "https://placeimg.com/640/360/any",
    sold: true,
    price: "584.00",
    brand: "Practical Steel Cheese",
    description:"Executive backing up Function-based",
    seller: "Leonor30",
    size:"M",
  },
  {
    id:"2",
    date:"2021-02-22T06:28:38.305Z",
    name:"Ms. Fiona Spencer",
    img:"https://placeimg.com/640/360/any",
    sold:true,
    price:"412.00",
    brand:"Tasty Plastic Shoes",
    description:"Lithuania",
    seller:"Ismael_Greenfelder",
    size:"S"
  }
];

test("renders correctly with no products", () => {
  const tree = create(<ProductsContainer products={[]} />).toJSON();
  expect(tree).toMatchSnapshot();
});

test("renders correctly with two products", () => {
  const tree = create(<ProductsContainer products={products} />).toJSON();
  expect(tree).toMatchSnapshot();
});
