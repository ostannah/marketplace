import { reducer } from "../components/ProductsContainer";

const state = [
  {
    id: '1',
  },
  {
    id: '2',
  }
];

test("Testing setLikedProducts: if found, item is removed from array.", () => {
  const item = {
    id: '1',
  };
  const result = reducer(state, item);
  console.log(result);
  expect(result.find((i) => i.id === item.id)).toBeUndefined();
});


test("Testing setLikedProducts: if found, item is added to array.", () => {
  const item = {
    id: '3',
  };
  const result = reducer(state, item);
  console.log(result);
  expect(result.find((i) => i.id === item.id)).toBeDefined();
});

