import "./ProductCard.css";
import styled from 'styled-components';
import { useState } from 'react';
import Heart from '../assets/Heart';
import DefaultImage from '../depop_logo.png';
import Loading from './Loading';

export const ProductCard = ({ product, toggleLike, isLiked }) => {

  const [loading, setLoading] = useState(true);
  
  return (
    <div className="productCardContainer">
      <div className="productCard">
        <div className="imageContainer">
          <img 
          src={(product && product.img) || DefaultImage} 
          onLoad={() => setLoading(false)}
          onError={(e) => e.target.src = DefaultImage}
          data-testid="thumbnail"
          alt="Product"
          />
          {loading ? 
            <Loading padding="15px 0px"/>
          : 
            <>
              <IconContainer>
                <Heart active={isLiked} onClick={toggleLike}/>
              </IconContainer>
              {product.sold && (
                <div className="soldOverlay">
                  <b>SOLD</b>
                </div>
              )}
            </>
          }
        </div>
        <div className="details">
          <span>{product && product.description}</span>
          <span>{product && product.brand}</span>
          <span>{product && product.size}</span>
          <span>
            <b>£{product && product.price}</b>
          </span>
        </div>
      </div>
    </div>
  );
};

const IconContainer = styled('div')`
  position: absolute;
  top: 5px;
  right: 5px;
  z-index: 1;
`;