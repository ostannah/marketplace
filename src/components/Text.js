import styled from 'styled-components';
import PropTypes from 'prop-types';

const getSize = (type) => {
  if (type === 'heading') return '20px';
  if (type === 'text') return '12px';
};

const getWeight = (type) => {
  if (type === 'heading') return 'bold';
};

const Text = styled('span')`
  font-size: ${({ type }) => getSize(type)};
  font-weight: ${({ type }) => getWeight(type)};
`;

export default Text;

Text.propTypes = {
  type: PropTypes.string,
};

Text.defaultProps = {
  type: "paragraph",
};