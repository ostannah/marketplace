import { useState, useContext } from "react";

import { ProductCard } from "./ProductCard";
import { useGetProducts } from "../hooks/useGetProducts";
import Flex from './Flex';
import Loading from './Loading';
import Button from './Button';
import Text from './Text';
import { AppContext } from '../App';

const ProductsContainer = () => {

  const { likedProducts, setLikedProducts } = useContext(AppContext);
  const [ hideSoldItems, setHideSoldItems ] = useState(false);

  const { products, loading } = useGetProducts(
    "https://5c78274f6810ec00148d0ff1.mockapi.io/api/v1/products", 
  );

  return (
    <>
      <Flex flexDirection='row' width='100%' flexWrap='wrap'>
        <Flex  padding='15px 15px 0px 15px' flexDirection='row' width='100%' justifyContent='space-between' alignItems='center'>
          <Text type='heading'>{products.length} Results</Text>
          <Button onClick={() => setHideSoldItems(!hideSoldItems)}> 
            {hideSoldItems ? 'Show' : 'Hide'} Sold Items
          </Button>
        </Flex>
        {loading && <Loading/>}
        {products && products.map((product) => {
          if (hideSoldItems && product.sold) return null;
          return (
            <ProductCard 
            key={product.id} 
            product={product} 
            toggleLike={() => setLikedProducts(product)}  
            isLiked={likedProducts.includes(product)}
            />
          );
        })}
      </Flex>
    </>
  );
};

export default ProductsContainer;
