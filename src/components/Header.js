import { useContext } from 'react';
import Dropdown from './Dropdown';
import { AppContext } from '../App';
import Flex from './Flex';
import Logo from '../assets/Logo';
import styled from 'styled-components';

export const Header = () => {

  const { likedProducts, setLikedProducts } = useContext(AppContext); 

  return (
    <StyledFlex flexDirection='row' justifyContent='space-between'>
      <LogoContainer>
        <Logo/>
      </LogoContainer>
      <Dropdown options={likedProducts} onClear={(option) => setLikedProducts(option)}/>
    </StyledFlex>
  );
};

export default Header;

const StyledFlex = styled(Flex)`
  background: white;
  position: sticky;
  top: 0;
  z-index: 2;
  padding: 15px;
  box-shadow: 0 5px 8px 0 rgba(0,0,0,0.04);
`;


const LogoContainer = styled('span')`
  width: 110px;
  border-radius: 10px;
`;


