import styled from 'styled-components';
import { useState, useEffect } from 'react';
import Heart from '../assets/Heart';
import Flex from './Flex';
import Text from './Text';
import Clear from '../assets/Clear';

export const Dropdown = ({ options = [], onClear }) => {
  
  const [ showOptions, setShowOptions ] = useState();

  useEffect(() => {
    if (!options.length) setShowOptions(false);
  }, [ options ]);

  return (
    <Flex flexDirection='column' alignItems='end'>
      <Heart active size='large' onClick={() => setShowOptions(!showOptions)} disabled={!options.length}/>
      {showOptions && options.length ?
        <Options flexDirection='column'>
          {options.map((option) => (
            <Option option={option} onClick={() => onClear(option)}/>
          ))}
        </Options>
      : null}
    </Flex>
  );
};

export default Dropdown;

const Option = ({ option, onClear }) => (
  <Flex key={option.id} justifyContent='space-between'>
    <Text>{option.name}</Text>
    <Clear onClick={() => onClear(option)}/>
  </Flex>
)

const Options = styled(Flex)`
  position: absolute;
  top: 45px;
  background-color: white;
  border-radius: 4px;
  border: solid 1px gray;
  width: 200px;
  div {
    padding: 5px;
    &:not(:last-of-type) {
      border-bottom: solid 1px gray;
    }
  }
  z-index: 3;
`;