import styled from 'styled-components';

const Button = styled('button')`
  display: Flex; 
  padding: 10px 5px;
  border-radius: 4px;
  &:hover {
    opacity: 0.8;
  }
  cursor: pointer;
  background-color: red;
  color: white;
  outline: none;
`;

export default Button;