import styled from 'styled-components';

const Flex = styled('div')`
  display: Flex; 
  flex-direction: ${({ flexDirection }) => flexDirection };
  flex-wrap: ${(flexWrap) => flexWrap};
  width: ${({ width }) => width };
  justify-content: ${({ justifyContent }) => justifyContent };
  align-items: ${({ alignItems }) => alignItems };
  margin: ${({ margin }) => margin };
  padding: ${({ padding }) => padding };
`;

export default Flex;